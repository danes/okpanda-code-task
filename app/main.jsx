/**
 * App entry point
 */

// Polyfill
import "babel-polyfill";

// Libraries
import React from "react";
import ReactDOM from "react-dom";
import { Router, browserHistory } from "react-router";

// Routes
import Routes from './common/components/Routes';

// Base styling
import "./common/base.css";

import usersApi from './apis/usersApi';
import timeslotsApi from './apis/timeslotsApi';
import lessonsApi from './apis/lessonsApi';


// ID of the DOM element to mount app on
const DOM_APP_EL_ID = "app";

if (process.env.NODE_ENV !== 'production') {
  // for console debugging
  window.api = {
    users: usersApi,
    timeslots: timeslotsApi,
    lessons: lessonsApi,
  };
}

// init big calendar localization
import moment from 'moment';
import BigCalendar from 'react-big-calendar';
BigCalendar.setLocalizer(
  BigCalendar.momentLocalizer(moment)
);

// Render the router
ReactDOM.render((
  <Router history={browserHistory}>
    {Routes}
  </Router>
), document.getElementById(DOM_APP_EL_ID));
