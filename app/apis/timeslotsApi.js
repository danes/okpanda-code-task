import axios from 'axios';
import getJson from './getJson';

export default {
  list: function(filter) {
    return axios.get('/api/timeslots', {
      params: filter,
    }).then(function(response){
      var data = response.data || {};

      return data.timeslots;
    });
  },

  create: function(params) {
    var { start_at, end_at } = params;

    return axios.post('/api/timeslots', {
      start_at: start_at,
      end_at: end_at,
    }).then(function(response){
      var data = response.data || {};
      return data.timeslot;
    });
  },
};