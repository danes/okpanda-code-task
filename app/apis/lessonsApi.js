import axios from 'axios';
import getJson from './getJson';

export default {

  list: function(filter) {
    return axios.get('/api/lessons', {
      params: filter,
    }).then(function(response){
      var data = response.data || {};

      return data.lessons;
    });
  },

  create: function(params) {
    var { start_at, end_at, teacher_id } = params;

    return axios.post('/api/lessons', {
      start_at: start_at,
      end_at: end_at,
      teacher_id: teacher_id,
    }).then(function(response){
      var data = response.data || {};
      return data.lesson;
    });
  },
};