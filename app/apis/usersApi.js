import axios from 'axios';
import getJson from './getJson';

export default {
  current: () => getJson('/api/users/current', 'user'),
  teachers: () => getJson('/api/users/teachers', 'users'),
  students: () => getJson('/api/users/students', 'users'),

  login: function(user) {
    var id = +((user && user.id) || user);

    return axios.post('/api/users/'+id+'/login').then(function(response){
      var data = response.data || {};

      var user = data.user;

      if (user) {
        return user;
      } else {
        throw new Error('User login failed');
      }
    });
  },
};