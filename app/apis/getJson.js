import axios from 'axios';

export default function(url, pluckKey) {
  return axios.get(url).then(function(response) {
    var json = response.data;

    if (pluckKey) {
      return json[pluckKey];
    } else {
      return json;
    }
  });
}
