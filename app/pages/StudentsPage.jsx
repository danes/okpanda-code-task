"use strict";

import React from "react";
import { resolve } from "react-resolver";
import usersApi from "../apis/usersApi";
import timeslotsApi from "../apis/timeslotsApi";
import lessonsApi from "../apis/lessonsApi";

import BigCalendar from 'react-big-calendar';
import _ from 'underscore';

var StudentsPage = React.createClass({

  getInitialState() {
    var teachers = this.props.teachers || [];

    return {
      currentTeacher: teachers[0],
    };
  },

  onTeacherClick(teacher) {
    this.setState({
      currentTeacher: teacher,
    });
  },

  render() {
    var {currentUser, teachers} = this.props;
    var {currentTeacher} = this.state;

    return (
      <div>
        <h1>Book a lesson</h1>
        <p>Current user: {currentUser.first_name} {currentUser.last_name}</p>

        <div className="row">
          <div className="col-md-3 col-sm-12">
            {this.renderTeacherSidebar(teachers, currentTeacher)}
          </div> 

          <div className="col-md-9 col-sm-12">
            {
              currentTeacher ?
                <TeacherCalendar teacher={currentTeacher} /> :
                null
            }
          </div>
        </div>
      </div>
    );
  },

  renderTeacherSidebar(teachers, currentTeacher) {
    teachers = teachers || [];

    return (
      <div>
        <h3>Teachers</h3>

        {teachers.map((teacher) =>
          <p key={teacher.id}><TeacherButton teacher={teacher} isActive={teacher === currentTeacher} onClick={this.onTeacherClick.bind(this, teacher)} /></p>
        )}

      </div>
    );
  },

});

var TeacherButton = function(props) {
  var { teacher, isActive, onClick } = props;

  var extraClass = isActive ? "btn-primary" : "btn-default";

  return (
    <button className={"btn btn-lg "+extraClass} onClick={onClick}>
      <i className="fa fa-user" />
      {teacher.first_name} {teacher.last_name}
    </button>
  );
};

var TeacherCalendar = React.createClass({
  propTypes: {
    teacher: React.PropTypes.object.isRequired,
  },

  getInitialState() {
    return {
      timeslots: [],
      lessons: [],
    };
  },

  fetchTimeslots(teacher) {
    teacher = teacher || this.props.teacher;

    this.setState(this.getInitialState());

    Promise.all([
      timeslotsApi.list({user_id: teacher.id}),
      lessonsApi.list()
    ]).then(function([timeslots, lessons]) {
      if (this.props.teacher === teacher) {
        this.setState({ 
          timeslots: timeslots,
          lessons: lessons,
        });
      }
    }.bind(this));
  },

  onSelectSlot(slotInfo) {
    var { teacher } = this.props;

    lessonsApi.create({
      start_at: slotInfo.start,
      end_at: slotInfo.end,
      teacher_id: teacher.id,
    }).then(() => {
      this.fetchTimeslots();
    });
  },

  componentDidMount() {
    this.fetchTimeslots();
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.teacher !== this.props.teacher) {
      this.fetchTimeslots(nextProps.teacher);
    }
  },

  render() {
    var {teacher} = this.props;
    var {timeslots, lessons} = this.state;

    var timeslotEvents = timeslots.map(function(timeslot){
      return {
        title: "Available",
        start: new Date(timeslot.start_at),
        end: new Date(timeslot.end_at),
      };
    });

    var timeslotIds = _(timeslots).pluck('id');
    var teacherLessons = _(lessons).filter(function(lesson) {
      return timeslotIds.indexOf(lesson.timeslot_id) >= 0;
    });

    var lessonEvents = teacherLessons.map(function(lesson){
      return {
        title: "Lesson",
        start: new Date(lesson.start_at),
        end: new Date(lesson.end_at),
      };
    });

    var events = timeslotEvents.concat(lessonEvents);

    return (
      <div>
        <h3>Availability for: {teacher.first_name} {teacher.last_name}</h3>

        <BigCalendar
          selectable
          events={events}
          defaultView='week'
          onSelectSlot={this.onSelectSlot}
        />

      </div>
    );
  }
});

export default resolve({
  currentUser: () => usersApi.current(),
  teachers: () => usersApi.teachers(),
})(StudentsPage);