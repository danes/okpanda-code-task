"use strict";

import React from "react";
import { resolve } from "react-resolver";
import usersApi from "../apis/usersApi";
import timeslotsApi from "../apis/timeslotsApi";

import BigCalendar from 'react-big-calendar';

var TeachersPage = React.createClass({

  getInitialState() {
    return {
      timeslots: [],
    };
  },

  fetchTimeslots() {
    var { currentUser } = this.props;
    
    timeslotsApi.list({user_id: currentUser.id}).then((timeslots) => {
      this.setState({ timeslots: timeslots });
    });
  },

  componentDidMount() {
    this.fetchTimeslots();
  },

  onSelectSlot(slotInfo) {
    console.log(slotInfo);

    timeslotsApi.create({
      start_at: slotInfo.start,
      end_at: slotInfo.end,
    }).then(() => {
      this.fetchTimeslots();
    });
  },

  render() {
    var {currentUser} = this.props;
    var {timeslots} = this.state;

    var events = timeslots.map(function(timeslot){
      return {
        title: "Available",
        start: new Date(timeslot.start_at),
        end: new Date(timeslot.end_at),
      };
    });

    console.log(events);

    return (
      <div>
        <h1>Teacher availability timeslots</h1>
        <p>Current user: {currentUser.first_name} {currentUser.last_name}</p>

        <BigCalendar
          selectable
          events={events}
          defaultView='week'
          onSelectEvent={event => alert(event.title)}
          onSelectSlot={this.onSelectSlot}
        />

      </div>
    );
  },
});

export default resolve({
  currentUser: () => usersApi.current(),
})(TeachersPage);