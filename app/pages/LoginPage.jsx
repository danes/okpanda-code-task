import React from "react";
import { resolve } from "react-resolver";
import usersApi from "../apis/usersApi";

var LoginPage = React.createClass({

  contextTypes: {
    router: React.PropTypes.object.isRequired
  },

  render() {
    var {teachers, students} = this.props;

    return (
      <div className="row">
        <div className="col-md-6 col-sm-12">
          <h1>User login</h1>
          <p>Select a user to login as:</p>

          <h3>Teachers</h3>
          {this.renderUsersList(teachers)}

          <h3>Students</h3>
          {this.renderUsersList(students)}

        </div>
      </div>
    );
  },

  loginUser(user) {
    usersApi.login(user).then(() => {
      console.log(user);

      if (user.role === 'teacher') {
        this.context.router.push('/teachers');  
      } else if (user.role === 'student') {
        this.context.router.push('/students');  
      }
    });
  },

  renderUsersList(users) {
    if (!users) { return null; }

    return (
      <div>
        {users.map((user) =>
          <p>{this.renderUserLoginButton(user)}</p>
        )}
      </div>

    );
  },

  renderUserLoginButton(user) {
    return (
      <button className="btn btn-sm" onClick={this.loginUser.bind(this, user)} key={user.id}>
        <i className="fa fa-user" />
        {user.first_name} {user.last_name}
      </button>
    );
  }
});

export default resolve({
  teachers: () => usersApi.teachers(),
  students: () => usersApi.students(),
})(LoginPage);