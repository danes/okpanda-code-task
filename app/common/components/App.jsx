import React from 'react';
import { Link } from 'react-router';

export default class App extends React.Component {
  render() {
    return (
      <div id="container">
        <nav className="navbar navbar-default">
          <ul className="nav navbar-nav">
            <li><Link to='/'>Users</Link></li>
            <li><Link to='/students'>Student's page</Link></li>
            <li><Link to='/teachers'>Teacher's page</Link></li>
          </ul>
        </nav>

        {this.props.children}
      </div>
    );
  }
}
