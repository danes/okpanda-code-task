import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './App';
import LoginPage from '../../pages/LoginPage';
import StudentsPage from '../../pages/StudentsPage';
import TeachersPage from '../../pages/TeachersPage';


export default (
  <Route path="/" component={App}>
    <IndexRoute component={LoginPage} />
    <Route path="students" component={StudentsPage} />
    <Route path="teachers" component={TeachersPage} />
  </Route>
);
