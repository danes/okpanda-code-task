var bookshelf = require('./bookshelf');

require('./User');
require('./Lesson');

var Timeslot = bookshelf.Model.extend({
  tableName: 'timeslots',
  hasTimestamps: true,

  user: function() {
    return this.belongsTo('User');
  },

  lessons: function() {
    return this.hasMany('Lesson');
  },

});

module.exports = bookshelf.model('Timeslot', Timeslot);
