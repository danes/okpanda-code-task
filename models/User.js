var bookshelf = require('./bookshelf');
var Checkit = require('checkit');

var User = bookshelf.Model.extend({
  tableName: 'users',
  hasTimestamps: true,

  validations: {
    first_name: ['required'],
    last_name: ['required'],
    email: ['required', 'email'],
    role: ['required'],
  },

  validate: function() {
    return new Checkit(this.validations).run(this.attributes);
  },

});

module.exports = bookshelf.model('User', User);
