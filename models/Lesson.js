var bookshelf = require('./bookshelf');

require('./User');
require('./Timeslot');

var Lesson = bookshelf.Model.extend({
  tableName: 'lessons',
  hasTimestamps: true,

  user: function() {
    return this.belongsTo('User');
  },

  timeslot: function() {
    return this.belongsTo('Timeslot');
  }

});

module.exports = bookshelf.model('Lesson', Lesson);
