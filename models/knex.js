var knexfile = require('../knexfile');
var knexConfig = process.env.NODE_ENV === 'production' ? knexfile.production : knexfile.development;

var knex = require('knex')(knexConfig);

module.exports = knex;