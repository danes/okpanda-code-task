"use strict";

var router = require('express').Router();
var Timeslot = require('../models/Timeslot');
var Lesson = require('../models/Lesson');
var bodyParser = require('body-parser');
var _ = require('underscore');

router.use(bodyParser.json());

router.use(function(req, res, next){
  req.fetchCurrentUser().then(function(user){
    req.currentUser = user;
    next();
  }).catch(next);
});

router.get('/', function(req, res, next){
  var user = req.currentUser;

  Lesson
  .where({user_id: user.id})
  .query(function(qb){
    return qb.orderBy('start_at');
  })
  .fetchAll()
  .then(function(lessons){
    res.json({
      lessons: lessons
    });
  });
});

router.post('/', function(req, res, next){
  var data = req.body || {};

  var teacher_id = data.teacher_id;
  var start_at = data.start_at;
  var end_at = data.end_at;

  var user = req.currentUser;

  Timeslot
    .where({user_id: teacher_id})
    .where('start_at', '<=', start_at)
    .where('end_at', '>=', end_at)
    .fetch().then(function(timeslot){
      if (!timeslot) { throw new Error("Can't book in this period"); }

      return new Lesson({
        user_id: user.id,
        timeslot_id: timeslot.id,
        start_at: start_at,
        end_at: end_at,
      }).save();
  }).then(function(lesson){
    res.json({
      lesson: lesson,
    });
  }).catch(next);
});

module.exports = router;