"use strict";

var router = require('express').Router();
var User = require('../models/User');
var Timeslot = require('../models/Timeslot');
var bodyParser = require('body-parser');
var _ = require('underscore');

router.use(bodyParser.json());

router.get('/', function(req, res, next){
  filteredTimeslots(req.query).fetchAll().then(function(timeslots){
    res.json({
      timeslots: timeslots,
    });
  }).catch(next);
});

router.get('/available', function(req, res, next){
  filteredTimeslots(req.query).fetchAll({withRelated: ['lessons']}).then(function(timeslots){
    res.json({
      timeslots: timeslots,
    });
  }).catch(next);
});

router.post('/', function(req, res, next){
  req.fetchCurrentUser().then(function(user){

    var data = req.body || {};

    var start_at = data.start_at;
    var end_at = data.end_at;

    return new Timeslot({
      user_id: user.id,
      start_at: start_at,
      end_at: end_at,
    }).save();
  }).then(function(timeslot){
    res.json({
      timeslot: timeslot,
    });
  }).catch(next);
});


function filteredTimeslots(filter) {
  filter = _(filter || {}).pick('user_id');

  return Timeslot.where(filter).query(function(qb){
    return qb.orderBy('start_at');
  });
}
module.exports = router;