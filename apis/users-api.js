"use strict";

var router = require('express').Router();
var User = require('../models/User');

router.get('/current', function(req, res, next){

  req.fetchCurrentUser().then(function(user){
    user = user || { guest: true };

    res.json({
      user: user,
    });
    
  });
});

router.post('/:id/login', function(req, res, next){
  var id = req.params.id;

  fetchUserById(id).then(function(user){
    
    if (!user) {

      res.json({
        error: "Invalid user",
      });

      return;
    }

    req.session.userId = user.id;

    res.json({
      ok: true,
      user: user,
    });

  }).catch(next);
});

router.get('/teachers', function(req, res, next){
  fetchUsersByRole('teacher').then(function(users){
    res.json({
      users: users,
    });
  }).catch(next);
});

router.get('/students', function(req, res, next){
  fetchUsersByRole('student').then(function(users){
    res.json({
      users: users,
    });
  }).catch(next);
});

function fetchUserById(id) {
  return new User({id: id}).fetch();
}

function fetchUsersByRole(role) {
  return User.where({role: role}).fetchAll();
}

module.exports = router;