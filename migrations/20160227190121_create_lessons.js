exports.up = function(knex, Promise) {
  return knex.schema.createTable('lessons', function (table) {
    table.increments();
    table.integer('user_id').references('users.id');
    table.integer('timeslot_id').references('timeslots.id');
    table.dateTime('start_at');
    table.dateTime('end_at');
    table.timestamps();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('lessons');
};
