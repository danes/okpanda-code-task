exports.up = function(knex, Promise) {
  return knex.schema.createTable('timeslots', function (table) {
    table.increments();
    table.integer('user_id').references('users.id');
    table.dateTime('start_at');
    table.dateTime('end_at');
    table.timestamps();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('timeslots');
};
