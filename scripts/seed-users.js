var User = require('../models/User');

var users = [
  new User({
    first_name: 'Johnny',
    last_name: 'English',
    email: 'johnny.english@example.com',
    role: 'teacher',
  }),
  new User({
    first_name: 'Walter',
    last_name: 'White',
    email: 'walter.white@example.com',
    role: 'teacher',
  }),
  new User({
    first_name: 'Mr.',
    last_name: 'Garrison',
    email: 'mr.garrison@example.com',
    role: 'teacher',
  }),  

  new User({
    first_name: 'Mary',
    last_name: 'Smith',
    email: 'mary.jane@example.com',
    role: 'student',
  }),

  new User({
    first_name: 'Jesse',
    last_name: 'Pinkman',
    email: 'jesse.pinkman@example.com',
    role: 'student',
  }),  

  new User({
    first_name: 'Eric',
    last_name: 'Cartman',
    email: 'eric.cartman@example.com',
    role: 'student',
  }),

];

Promise.all(users.map(function(user){
  return user.save();
})).then(function(users){
  console.log('Seeded users', users.map(function(user){
    return user.toJSON();
  }));

  process.exit();
}).catch(function(e){
  console.log('An error occured: ', e);
  process.exit();
});