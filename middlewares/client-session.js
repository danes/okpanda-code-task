var sessions = require("client-sessions");

module.exports = sessions({
  cookieName: 'session', // cookie name dictates the key name added to the request object
  secret: 'b92782693822c02b25b8a1599673ad92efb12797f9f386326c448bd543a550c7', // should be a large unguessable string
  duration: 24 * 60 * 60 * 1000, // how long the session will stay valid in ms
  activeDuration: 1000 * 60 * 5 // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds
});