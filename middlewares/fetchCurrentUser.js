var User = require('../models/User');

function fetchUserById(id) {
  return new User({id: id}).fetch();
}

module.exports = function(req, res, next){
  req.fetchCurrentUser = function() {
    var userId = req.session.userId;
    
    if (!userId) {
      return Promise.resolve(null);
    } else {
      return fetchUserById(userId);  
    }
  };

  next();
};