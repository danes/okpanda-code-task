"use strict";

var express = require('express');
var app = express();
var usersApi = require('./apis/users-api');
var timeslotsApi = require('./apis/timeslots-api');
var lessonsApi = require('./apis/lessons-api');

var isProduction = process.env.NODE_ENV === 'production';

/************************************************************
 *
 * Express routes for:
 *   - app.js
 *   - style.css
 *   - index.html
 *
 ************************************************************/

// add req.fetchCurrentUser helper
app.use(require('./middlewares/fetchCurrentUser'));

// Serve application file depending on environment
app.get('/app.js', function(req, res) {
  if (isProduction) {
    res.sendFile(__dirname + '/build/app.js');
  } else {
    res.redirect('//localhost:9090/build/app.js');
  }
});

// Serve aggregate stylesheet depending on environment
app.get('/style.css', function(req, res) {
  if (isProduction) {
    res.sendFile(__dirname + '/build/style.css');
  } else {
    res.redirect('//localhost:9090/build/style.css');
  }
});

app.use(express.static('public'));

app.use(require('./middlewares/client-session'));

app.use('/api/users', usersApi);
app.use('/api/timeslots', timeslotsApi);
app.use('/api/lessons', lessonsApi);

// Serve index page
app.get('*', function(req, res) {
  res.sendFile(__dirname + '/build/index.html');
});


/******************
 *
 * Express server
 *
 *****************/

var port = process.env.PORT || 8080;
var server = app.listen(port, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Essential React listening at http://%s:%s', host, port);
});
