Features
--------

- built with React and PostgreSQL backend
- user session handling with backend models and user roles
- display and creation of timeslots for teachers
- display of availability and booking of lessons for students
- deployed in production on Heroku:
  [https://scheduling-code-task.herokuapp.com](https://scheduling-code-task.herokuapp.com)


TODO
----

- confirmation dialogs
- prevent double-booking in the same timeslot
- prevent overlapping timeslots (e.g. by merging into one single timeslot)
- don't display booked parts of timeslots
- more robust validation