// Update with your config settings.

module.exports = {

  development: {
    client: 'postgresql',
    connection: {
      database: 'okp',
      user:     'okp',
      password: 'okp'
    },
    pool: {
      min: 2,
      max: 5
    },
    migrations: {
      tableName: 'knex_migrations'
    },
    debug: true,
  },

  production: {
    client: 'postgresql',
    connection: process.env.DATABASE_URL,
    pool: {
      min: 2,
      max: 5
    },
    migrations: {
      tableName: 'knex_migrations'
    },
    debug: true,
  }

};
